//
// Created by DrMark on 10/2/2017.
//

#ifndef TEXTADV_STATE_H
#define TEXTADV_STATE_H

#include "Room.h"
#include "item.h"

class State {
  Room *currentRoom;

 public:
  State();
  std::vector<CurribleItem> items;
  void goTo(Room *target);
  void announceLoc() const;
  Room *getCurrentRoom() const;
  void pack(auto... args);
  void save(std::string file_path = "assests/state.json");
  auto load(std::string file_path = "assests/state.json") -> int;
  const CurribleItem &getItemByKeyWord(auto keyword);
};

#endif  // TEXTADV_STATE_H
