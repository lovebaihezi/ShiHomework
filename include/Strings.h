//
// Created by DrMark on 10/4/2017.
//

#ifndef TEXTADV_STRINGS_H
#define TEXTADV_STRINGS_H

#include <string>

const std::string r1name = "Room 1";
const std::string r1desc =
    "You are in room 1. It's really quite boring, but then, it's just for "
    "testing really. There's a passage to the north.";
const std::string r2name = "Blue Room";
const std::string r2desc =
    "You are the blue room. You know because it's blue. That's about all "
    "though. There's a passage to the south.";
const std::string r3name = "Red Room";
const std::string r3desc =
    "You are the red room. You want to leave here.There's a passage to the "
    "west.";
const std::string r4name = "Black Room";
const std::string r4desc =
    "You are the black room. You feel tired,but you see an exit to the east.";
const std::string r5name = "Safe Room";
const std::string r5desc = "You decide to take a break.";

const std::string badExit = "You can't go that way.";
const std::string badCommand = "I don't understand that.";

#endif  // TEXTADV_STRINGS_H
