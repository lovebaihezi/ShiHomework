#ifndef ITEM
#define ITEM
#include <string>
#include <string_view>
#include <vector>
class Item {
 public:
  Item() = default;
  virtual ~Item() = default;
};

class CurribleItem : public Item {
 public:
  std::string keyword;
  std::string name;
  std::string description;
  CurribleItem() = delete;
  CurribleItem(std::string _name) : name{_name} {}
  CurribleItem& addKeyWord(std::string keyword);
  CurribleItem& addDescription(std::string des);
  virtual ~CurribleItem() = default;
  inline bool operator==(const auto& rhs) { return this->keyword == rhs; }
};
#endif  // !ITEM
