//
// Created by DrMark on 10/2/2017.
//

#ifndef TEXTADV_ROOM_H
#define TEXTADV_ROOM_H

#include <forward_list>
#include <memory>
#include <mutex>
#include <span>
#include <string>
#include <string_view>
#include <vector>

#include "item.h"

using std::string;

/**
 * Represents a room (accessible location in the game).
 */
class Room {
  /**
   * Short name used as a header.
   */
  const string name;
  /**
   * Full description of the room.
   */
  const string description;
  /**
   * Pointer to room that is north of this one.
   */
  Room* north{nullptr};
  Room* south{nullptr};
  Room* west{nullptr};
  Room* east{nullptr};

 public:
  /**
   * Constructs a new Room.
   * @param _name Name of the room.
   * @param _desc Description of the room.
   */
  Room(const string&& _name, const string&& _desc);

  /**
   * Removes a destroyed room from the global list if it's there.
   */
  ~Room() = default;

  /**
   * Outputs the name and description of the room
   * in standard format.
   */
  void describe() const;

  /**
   * List storing all rooms that have been registered via addRoom().
   */
  static std::vector<Room*> rooms;

  std::vector<CurribleItem> items;

  void packItemByKeyWord(std::string_view keyword,
                         std::vector<CurribleItem>* items);
  /**
   * Creates a new Room with the given parameters and register it with the
   * static list.
   * @param _name Name of the room.
   * @param _desc Description of the room.
   */
  static Room* addRoom(const string&& _name, const string&& _desc);
  static void addRoom(Room* room);

  Room* getNorth() const;
  void setNorth(Room* _north);

  Room* getSouth() const;
  void setSouth(Room* _south);

  Room* getWest() const;
  void setWest(Room* _west);

  Room* getEast() const;
  void setEast(Room* _east);
  CurribleItem& emplaceBack(auto&&... args);
  CurribleItem& pushBack(CurribleItem&& args);
  void static load(std::string file_path = "assests/rooms.json");
};

#endif  // TEXTADV_ROOM_H
