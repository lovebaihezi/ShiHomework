cmake_minimum_required(VERSION 3.20)
project(Game CXX C) 
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address -fsanitize=leak -fsanitize=undefined -Wall")
# set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -stdlib=libc++ -lc++abi -fuse-ld=mold")
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++2b -stdlib=libc++ -march=native")
include_directories(
  ./include
)
set(CMAKE_CXX_STANDARD 23)
find_package(fmt CONFIG REQUIRED)
find_package(RapidJSON CONFIG REQUIRED)
set(CMAKE_EXPORT_COMPILE_COMMANDS Yes)
set(LINKS fmt::fmt-header-only rapidjson)
file(GLOB GAME_HOMEWORK_SRC src/Game/**.cpp)
file(GLOB GAME_HOMEWORK_HEADRS include/Game/**.h)
file(GLOB GAME_HOMEWORK_TESTS tests/Game/**.cpp)

add_executable(Game ${GAME_HOMEWORK_SRC} ${GAME_HOMEWORK_HEADRS})
target_link_libraries(Game PUBLIC ${LINKS} -static)
