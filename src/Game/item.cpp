#include "item.h"

#include <algorithm>

CurribleItem& CurribleItem::addKeyWord(std::string keyword) {
  this->keyword = keyword;
  return *this;
}

CurribleItem& CurribleItem::addDescription(std::string des) {
  this->description = des;
  return *this;
}
