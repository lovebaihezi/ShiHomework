//
// Created by DrMark on 10/2/2017.
//

#include "State.h"

#include <algorithm>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>

#include "fmt/format.h"
#include "rapidjson/document.h"
#include "rapidjson/filewritestream.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/writer.h"

/**
 * Current state of the game.
 */

/**
 * Display the description of the room the player is in. */

void State::announceLoc() const { this->currentRoom->describe(); }

/**
 * Constructor.
 * @param startRoom Pointer to the room to start in.
 */
State::State() : currentRoom(){};

/**
 * Move to a specified room and print its description.
 * @param target Pointer to the room to move to.
 */
void State::goTo(Room* target) {
  this->currentRoom = target;
  this->announceLoc();
}

/**
 * Return a pointer to the current room.
 * @return Pointer to the current room.
 */
Room* State::getCurrentRoom() const { return this->currentRoom; }

void State::pack(auto... args) { this->items.emplace_back(args...); }

const CurribleItem& State::getItemByKeyWord(auto keyword) {
  for (auto i = 0; i < items.size(); i += 1) {
    if (items[i] == keyword) {
      return items[i];
    }
  }
}

using namespace rapidjson;

void State::save(std::string file_path) {
  std::ofstream ofs(file_path);
  OStreamWrapper osw(ofs);
  Writer<OStreamWrapper> writer(osw);
  auto place = std::find(Room::rooms.begin(), Room::rooms.end(), currentRoom);
  int loc = place - Room::rooms.begin();
  Document json;
  json.SetObject();
  Value location(loc);
  auto& allocator = json.GetAllocator();
  json.AddMember("location", location.Move(), allocator);
  Value gameObjects(kArrayType);
  for (auto& item : items) {
    gameObjects.PushBack(
        Value()
            .SetString(item.keyword.data(), item.keyword.size(), allocator)
            .Move(),
        allocator);
  }
  json.AddMember("gameObjects", gameObjects.Move(), allocator);
  json.Accept(writer);
  ofs.close();
}

auto State::load(std::string file_path) -> int {
  std::ifstream ifs;
  ifs.open(file_path);
  Document document;
  IStreamWrapper isw(ifs);
  document.ParseStream(isw);
  int location = 0;
  if (document["location"].IsInt()) {
    location = document["location"].GetInt();
  }
  assert(document["gameObjects"].IsArray() &&
         "field gameObjects is not an array");
  const Value& gameObjects = document["gameObjects"];
  for (const auto& item : gameObjects.GetArray()) {
    auto str = std::string{item.GetString(), item.GetStringLength()};
    this->pack(str);
  }
  currentRoom = Room::rooms[location];
  return location;
}
