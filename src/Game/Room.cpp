//
// Created by DrMark on 10/2/2017.
//

#include "Room.h"

#include <fstream>

#include "Wordwrap.h"
#include "fmt/core.h"
#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/rapidjson.h"
// #include "fmt/format.h"

/**
 * Stores a static list of all rooms.
 */
std::mutex rooms_mutex{};
std::vector<Room *> Room::rooms{};
/**
 * Room default constructor.
 * @param _name Room's name.
 * @param _desc Room's description.
 */
Room::Room(const string &&_name, const string &&_desc)
    : name(_name), description(_desc) {};
/**
 * Prints the description of a room (the name and long description)
 */
void Room::describe() const {
  wrapOut(&name);
  wrapEndPara();
  wrapOut(&description);
  wrapEndPara();
  if (items.size() != 0) {
    std::string s{"|item name|"};
    wrapOut(&s);
    wrapEndPara();
    for (auto &item : items) {
      std::string str = fmt::format("| {} |", item.name);
      wrapOut(&str);
      wrapEndPara();
    }
  } else {
    std::string s{"current room has no item!"};
    wrapOut(&s);
    wrapEndPara();
  }
}

/**
 * Statically creates a room and then adds it to the global list.
 * @param _name Name for the new room.
 * @param _desc Description for the new room.
 * @return A pointer to the newly created room.
 */
Room *Room::addRoom(const string &&_name, const string &&_desc) {
  while (true) {
    if (rooms_mutex.try_lock()) {
      auto newRoom = new Room(std::move(_name), std::move(_desc));
      Room::rooms.push_back(newRoom);
      rooms_mutex.unlock();
      return newRoom;
    }
  }
}

/**
 * Adds an existing room to the static list.
 * @param room Pointer to the room to add.
 * @return
 */
void Room::addRoom(Room *room) {
  while (true) {
    if (rooms_mutex.try_lock()) {
      Room::rooms.push_back(room);
      rooms_mutex.unlock();
      break;
    }
  }
}

/**
 * Gets the north exit of a room.
 * @return The room that is north of this one, or NULL if there is no exit.
 */
Room *Room::getNorth() const { return this->north; }

/**
 * Sets the north exit of a room.
 * @param _north Pointer to the room to be north of this one.
 */
void Room::setNorth(Room *_north) { this->north = _north; }

Room *Room::getEast() const { return this->east; }

void Room::setEast(Room *_east) { this->east = _east; }

Room *Room::getSouth() const { return this->south; }

void Room::setSouth(Room *_south) { this->south = _south; }

Room *Room::getWest() const { return this->west; }

void Room::setWest(Room *_west) { this->west = _west; }

CurribleItem &Room::emplaceBack(auto &&...args) {
  items.emplace_back(args...);
  return items.back();
}

CurribleItem &Room::pushBack(CurribleItem &&item) {
  items.push_back(std::move(item));
  return items.back();
}

void Room::packItemByKeyWord(std::string_view keyword,
                             std::vector<CurribleItem> *arr) {
  for (auto &item : *arr) {
    if (item == keyword) {
      auto info = std::string("you already pack it!");
      wrapOut(&info);
      wrapEndPara();
    }
  }
  auto input = false;
  for (auto i = 0; i < this->items.size(); i += 1) {
    if (this->items[i] == keyword) {
      auto value = this->items[i];
      input = true;
      arr->push_back(value);
      for (auto j = i; j < this->items.size() - 1; j += 1) {
        this->items[j] = std::move(this->items[j + 1]);
      }
    }
  }
  if (!input) {
    auto info = std::string("current room dose not have item which is ");
    info.append(keyword);
    wrapOut(&info);
    wrapEndPara();
  }
}

using namespace rapidjson;
void Room::load(std::string file_path) {
  std::ifstream ifs(file_path);
  IStreamWrapper isw(ifs);
  Document doc;
  doc.ParseStream(isw);
  auto &Rooms = doc["rooms"];
  auto &GameObjects = doc["gameObjects"];
  auto GameObjectsArray = GameObjects.GetArray();
  assert(Rooms.IsArray());
  auto game_obj = std::vector<CurribleItem>{};
  for (const Value &v : GameObjects.GetArray()) {
    auto name = std::string{v["name"].GetString(), v["name"].GetStringLength()};
    auto des = std::string{v["description"].GetString(),
                           v["description"].GetStringLength()};
    auto keyword =
        std::string{v["keyword"].GetString(), v["keyword"].GetStringLength()};
    CurribleItem item(name);
    item.addKeyWord(keyword).addDescription(des);
    game_obj.emplace_back(std::move(item));
  }
  for (const Value &v : Rooms.GetArray()) {
    auto name = std::string{v["name"].GetString(), v["name"].GetStringLength()};
    auto des = std::string{v["description"].GetString(),
                           v["description"].GetStringLength()};
    Room *room = new Room(std::move(name), std::move(des));
    Room::addRoom(room);
  }
  for (auto i = 0; i < rooms.size(); i += 1) {
    const Value &v = Rooms[i];
    if (auto result = v.FindMember("north");
        result != v.MemberEnd() && result->value.IsInt()) {
      int x = v["north"].GetInt();
      Room *room = rooms[x];
      rooms[i]->setNorth(room);
    }
    if (auto result = v.FindMember("south");
        result != v.MemberEnd() && result->value.IsInt()) {
      int x = v["south"].GetInt();
      Room *room = rooms[x];
      rooms[i]->setSouth(room);
    }
    if (auto result = v.FindMember("west");
        result != v.MemberEnd() && result->value.IsInt()) {
      int x = v["west"].GetInt();
      Room *room = rooms[x];
      rooms[i]->setWest(room);
    }
    if (auto result = v.FindMember("east");
        result != v.MemberEnd() && result->value.IsInt()) {
      int x = v["east"].GetInt();
      Room *room = rooms[x];
      rooms[i]->setEast(room);
    }
    if (auto result = v.FindMember("gameObjects");
        result != v.MemberEnd() && result->value.IsArray()) {
      for (const Value &v : v["gameObjects"].GetArray()) {
        int x = v.GetInt();
        rooms[i]->pushBack(std::move(game_obj[x]));
      }
    }
  }
}
