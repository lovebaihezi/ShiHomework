
#include <algorithm>
#include <any>
#include <array>
#include <cassert>
#include <cctype>
#include <chrono>
#include <concepts>
#include <forward_list>
#include <functional>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <locale>
#include <memory>
#include <optional>
#include <random>
#include <span>
#include <thread>
#include <tuple>
#include <type_traits>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <utility>
#include <variant>
#include <vector>

#include "Room.h"
#include "State.h"
#include "Strings.h"
#include "Wordwrap.h"
#include "fmt/core.h"
using std::string;
// #include <range/v3/algorithm.hpp>
// #include <ranges>

/**
 * Print out the command prompt then read a command into the provided string
 * buffer.
 * @param buffer Pointer to the string buffer to use.
 */
void inputCommand(string *buffer) {
  buffer->clear();
  std::cout << "> ";
  std::getline(std::cin, *buffer);
}

/**
 * Sets up the map.
 */
void initRooms() {
  using namespace std;
  // auto *r2 = new Room(&r2name, &r2desc);
  // auto *r1 = new Room(&r1name, &r1desc);
  // auto *r3 = new Room(&r3name, &r3desc);
  // auto *r4 = new Room(&r4name, &r4desc);
  // auto *r5 = new Room(&r5name, &r5desc);
  // Room::addRoom(r1);
  // Room::addRoom(r2);
  // Room::addRoom(r3);
  // Room::addRoom(r4);
  // Room::addRoom(r5);
  // r1->setNorth(r2);
  // auto item1 = CurribleItem("sword"s);
  // item1.addDescription("a simple item, useless, skip it"s);
  // item1.addKeyWord("weapon"s);
  // r1->pushBack(std::move(item1));
  // auto item2 = CurribleItem("shield"s);
  // item2.addDescription("a simple item, useless, skip it twice"s);
  // item2.addKeyWord("armor"s);
  // r1->pushBack(std::move(item2));
  // r2->setSouth(r3);
  // r3->setWest(r4);
  // r4->setEast(r5);
  // return r1;
  Room::load();
}

/**
 * Sets up the game state.
 */
std::unique_ptr<State> initState() {
  auto state = std::make_unique<State>();
  fmt::print("{}\n", state->load());
  assert(state->getCurrentRoom() != nullptr && "state init failed");
  return state;
}

enum class Commands : int {
  None = 0,
  Start = 1,
  GoNorth = 2,
  GoSouth = 3,
  GoWest = 4,
  GoEast = 5,
  Quit = 6,
  PackItemFromRoom = 7,
  Inventory = 8,
  Examine = 9,
  DropItem = 10,
  Save = 11,
  Load = 12,
};

template <typename TCommandLike>
concept CommandLike = requires(TCommandLike t) {
  { t.type() } -> std::same_as<Commands>;
};

class InventoryCommand {
  Commands command_type;
  std::string keyword;

 public:
  InventoryCommand(Commands type, std::string data)
      : command_type(type), keyword{std::move(data)} {}
  [[nodiscard]] auto type() const -> const Commands & { return command_type; }
  [[nodiscard]] auto data() const -> const std::string & { return keyword; }
};

using CommandType = std::variant<Commands, InventoryCommand>;

class CommandMap {
  std::unordered_map<std::string, Commands> map;

 public:
  CommandMap() {
    map.insert({"north", Commands::GoNorth});
    map.insert({"south", Commands::GoSouth});
    map.insert({"west", Commands::GoWest});
    map.insert({"east", Commands::GoEast});
    map.insert({"quit", Commands::Quit});
    map.insert({"n", Commands::GoNorth});
    map.insert({"s", Commands::GoSouth});
    map.insert({"w", Commands::GoWest});
    map.insert({"e", Commands::GoEast});
    map.insert({"q", Commands::Quit});
    map.insert({"p", Commands::PackItemFromRoom});
    map.insert({"inventory", Commands::Inventory});
    map.insert({"pack", Commands::PackItemFromRoom});
    map.insert({"drop", Commands::DropItem});
    map.insert({"examine", Commands::Examine});
  }
  ~CommandMap() = default;
  auto has(auto key) const -> bool { return map.count(key) != 0; }
  auto at(auto key) const -> CommandType {
    Commands v = map.at(key);
    return {v};
  }
  auto at(const std::string &cmd, const std::string &keyword) const
      -> CommandType {
    if (keyword.empty()) {
      return this->at(cmd);
    }
    Commands v = map.at(cmd);
    return {InventoryCommand{v, keyword}};
  }
};

template <typename Adapter, typename Result>
concept CommandAdapterTrait = requires(Adapter adapter) {
  { adapter.next() } -> std::same_as<Result>;
};

template <typename Map, typename... TInput, typename TCommand>
concept CommandCommandMap = requires(Map map, TInput... input) {
  { map.at(input...) } -> std::same_as<TCommand>;
  { map.has(input...) } -> std::same_as<bool>;
};

class StdInput {
  std::string buffer;

  static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](auto v) { return !std::isspace(v); }));
  }

  // trim from end (in place)
  static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](auto v) { return !std::isspace(v); })
                .base(),
            s.end());
  }

 public:
  auto next() -> std::string {
    this->buffer.clear();
    std::cout << "(|> ";
    std::getline(std::cin, this->buffer);
    ltrim(this->buffer);
    rtrim(this->buffer);
    return this->buffer;
  }
};

// FIXME: Could accpect class with begim, end defiend even it is not an
// iterator;
template <typename Container, typename TCommand>
concept MutableIteratorable = requires(Container container, TCommand t) {
  { container.begin() } -> std::same_as<typename Container::iterator>;
  { container.end() } -> std::same_as<typename Container::iterator>;
  {container.push_back(std::move(t))};
  { container.back() } -> std::same_as<TCommand &>;
};

template <typename P, typename TInput>
concept CParser = requires(P p, TInput s) {
  {p.parse(s)};
};

class Parser {
 public:
  static auto parse(const std::string &str)
      -> std::pair<std::string, std::optional<std::string>> {
    auto b = str.find_first_of(' ');
    auto e = std::find_if(str.begin() + b, str.end(),
                          [](char v) { return v == ' '; });
    if (b == std::string::npos || e == str.end()) {
      return {{str}, {}};
    }
    return {{str.begin(), str.begin() + b}, {{e + 1, str.end()}}};
  }
};

// FIXME: Make sure no vector<bool> here
template <typename TInput, typename TCommand,
          MutableIteratorable<TCommand> Container = std::vector<TCommand>>
class Commander {
  Container cmdList{};

 public:
  Commander() = default;
  explicit Commander(Container &&origin) : cmdList(origin) {}
  template <std::size_t L = std::dynamic_extent>
  explicit Commander(std::span<TCommand, L> events) {
    for (auto &&event : events) {
      cmdList.push_back(event);
    }
  }
  template <typename... Args>
  explicit Commander(Args &&...args) : cmdList({args...}) {}
  Commander(Commander &self) = delete;
  Commander(Commander &&self) noexcept = default;
  // auto emit(CommandAdapterTrait<TInput> auto &adapter,
  //           auto &parser,
  //           CommandCommandMap<TInput, TCommand> auto &cem) -> const TCommand & {
    auto emit(auto &adapter, auto &parser, auto &cem) -> const TCommand & {
    TInput input = adapter.next();
    auto [cmd, keyword] = parser.parse(input);
    if (cem.has(cmd)) {
      auto v = keyword ? cem.at(cmd, keyword.value()) : cem.at(cmd);
      cmdList.push_back(std::move(v));
      return cmdList.back();
    } else {
      wrapOut(&badCommand);
      wrapEndPara();
      return emit(adapter, parser, cem);
    }
  }
  [[nodiscard]] const TCommand &getCommand() const { return cmdList.back(); }
  TCommand Drop() {
    auto event = TCommand{this->eventList.back()};  // copy data from reference
    this->eventList.pop_back();
    return event;
  }
};

void handleMove(auto &state, Room *direction) {
  if (direction != nullptr) {
    state->goTo(direction);
  } else {
    wrapOut(&badExit);
    wrapEndPara();
  }
}

/**
 * The main game loop.
 */
auto gameLoop(std::unique_ptr<State> &&state) noexcept -> void {
  Commander<std::string, CommandType> commander{Commands::Start};
  StdInput input;
  Parser parser;
  CommandMap map;
  for (;;) {
    const CommandType &last_command = commander.getCommand();
    if (std::holds_alternative<Commands>(last_command) &&
        std::get<Commands>(last_command) == Commands::Quit) {
      break;
    }
    const auto &cmd = commander.emit(input, parser, map);
    Room *room = state->getCurrentRoom();
    if (std::holds_alternative<Commands>(cmd)) {
      switch (std::get<Commands>(cmd)) {
        case Commands::GoNorth:
          handleMove(state, room->getNorth());
          break;
        case Commands::GoSouth:
          handleMove(state, room->getSouth());
          break;
        case Commands::GoEast:
          handleMove(state, room->getEast());
          break;
        case Commands::GoWest:
          handleMove(state, room->getWest());
          break;
        case Commands::Inventory: {
          if (!state->items.empty()) {
            std::string s{"| item name | item description | item key word |"};
            wrapOut(&s);
            wrapEndPara();
            for (const auto &item : state->items) {
              std::string str = fmt::format("| {} | {} | {} |", item.name,
                                            item.description, item.keyword);
              wrapOut(&str);
              wrapEndPara();
            }
          } else {
            std::string s{"you don't have any items!"};
            wrapOut(&s);
            wrapEndPara();
          }
        } break;
        case Commands::PackItemFromRoom:
        case Commands::DropItem:
        case Commands::Examine: {
          std::string info{
              "you need to spefic second paramaters to use such command"};
          wrapOut(&info);
          wrapEndPara();
        } break;
        default:
          break;
      }
    } else if (std::holds_alternative<InventoryCommand>(cmd)) {
      auto &v_cmd = std::get<InventoryCommand>(cmd);
      auto data = v_cmd.data();
      auto type = v_cmd.type();
      switch (type) {
        case Commands::PackItemFromRoom: {
          room->packItemByKeyWord(std::string{data}, &state->items);
          std::string s{"|all my item name|"};
          wrapOut(&s);
          wrapEndPara();
          for (const auto &item : state->items) {
            std::string str = fmt::format("| {} |", item.name);
            wrapOut(&str);
            wrapEndPara();
          }
        } break;
        case Commands::DropItem: {
          for (auto i = 0; i < state->items.size(); i += 1) {
            if (state->items[i] == data) {
              auto item = state->items[i];
              room->pushBack(std::move(item));
              for (auto j = i; j < state->items.size() - 1; j += 1) {
                state->items[j] = state->items[j + 1];
              }
              state->items.pop_back();
            }
          }
        } break;
        case Commands::Examine: {
          auto iter = std::find(state->items.begin(), state->items.end(), data);
          if (iter == state->items.end()) {
            auto info = std::string{"you don't have item which keyword is {}"};
            info.append(data);
            wrapOut(&info);
            wrapEndPara();
          } else {
            wrapOut(&iter->description);
            wrapEndPara();
          }
        } break;
        default:
          break;
      }
    }
  }
  state->save();
}

int main(int argc, const char *args[]) {
  initRooms();
  auto status = initState();
  status->announceLoc();
  gameLoop(std::move(status));
  for (auto *room : Room::rooms) {
    delete room;
  }
  return 0;
}
